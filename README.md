
### 
Simple Usage Jalali DatePicker.

![alt text](screenshots/01.png)
![alt text](screenshots/02.png)

### Usage
```html
<input class="form-control" mask="yyyy/MM/dd" mask-placeholder="____/__/__" placeholder="Date" #myDateVar (onchange)="myDate = $event" />

<jdatepicker [input]="myDateVar"></jdatepicker>
```
### Examples

##### *.component.ts
```typescript
@Component(/**/)
export class Component implements OnInit {

  myDate:string = 'yyyy/MM/dd';

  public constructor(private service: Service) { }
  
  ngOnInit(): void { 


  
  }
   getDate() {
    this.progressDate=true;
    console.log(this.myDate);
  }
}
```
##### *.component.html
```html
<input class="form-control" mask="yyyy/MM/dd" mask-placeholder="____/__/__" placeholder="Date" #myDateVar (onchange)="myDate = $event" />

<jdatepicker [input]="myDateVar"></jdatepicker>


 <div><button (click)="getDate()">getDate</button></div>

```