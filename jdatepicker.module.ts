import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { JDatepickerComponent } from './jdatepicker.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,

  ],
  declarations: [
    JDatepickerComponent
  ],
  exports: [
    JDatepickerComponent
  ]
})
export class JDatepickerModule { }
